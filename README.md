# whendo

WHENDO, A UNIXY, LITTLE EVENT REACTOR

# extending almighty cron 

Cron only runs maximum *every minute*, which sometimes is just not going to cut it.
However, running whendo as a cronjob, will extend this to *every second* (default: 3seconds) :

```
* * * * * /path/to/whendo /myproject/.whendo
```

> This allows writing testscripts (calling `whendo <whendo-dir>`) *much* easier (compared to checking cron-logs every *MINUTE*).
